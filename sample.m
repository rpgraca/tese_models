function y = sample(t,x,tsamp,tinit)
tact = tinit;
i=1;
for j = 1:length(t)
    if(t(j)>tact)
        y(i) = x(j);
        i=i+1;
        tact = tact + tsamp;
    end
end
end
