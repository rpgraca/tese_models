function selected = bist(dist,tol)
    selected = 1:length(dist);
    old_selected = 0;
    min_dist = min(dist);
    max_dist = max(dist);
    fignum = 1;
    while(1) 
        figure
        hist(dist(selected),ceil(100*(max(dist(selected))-min(dist(selected)))/(max_dist-min_dist)))
        axis([min_dist max_dist]);
        title(fignum);
        fignum=fignum+1;
        avg = mean(dist(selected));
        old_selected = selected;
        selected = 0;
        j=1;
        for i=1:length(dist)
            if(abs(dist(i)-avg) < tol)
                selected(j) = i;
                j=j+1;
                %fprintf('%d selected\n',i);
            else
                %fprintf('%d not selected\n',i);
            end
        end
        if(length(selected) == length(old_selected))
            if (selected == old_selected)
                break
            end
        end
    end
end
