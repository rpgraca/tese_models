clear
w1 = 220;
w2 = 720;
duty = 1/1000;
fsamp = 2000;
delta = duty/fsamp;
tau1 = duty/w1;
tau2 = duty/w2;
a = exp(-delta/tau1);
b = exp(-delta/tau2);
period=0.25*fsamp;
qm=5;
kgm = 3;
kmg = 1;

fin = 100;

%in = sin(2*pi*fin*t);
in = [ones(1,period) zeros(1,period) ones(1,period) zeros(1,period)];
t = 0:1/fsamp:(length(in))/fsamp;

m(1) = 0;
mm(1) = 0;
outm(1) = 0;
g(1) = 0;
mg(1) = 0;
outg(1) = 0;
xx = log(1-qm*log(1+1/qm));

for i = 1:length(in)
    inm(i) = (in(i)+kmg*outg(i));
    mm(i+1) = (1-a)*inm(i) + a*mm(i);
    m(i+1) = (1/(tau1-tau2)*(tau1*(1-a)-tau2*(1-b)))*inm(i) + b*m(i) + tau1/(tau1-tau2)*(a-b)*mm(i);
    %m(i+1) = (1-b)*mm(i) + b*m(i);
    if(m(i+1)<xx)
        outm(i+1)=-1;
    else
        outm(i+1)=qm*(1-exp(-(exp(m(i+1))-1)/qm));
    end
    ing(i) = kgm*outm(i);
    mg(i+1) = (1-a)*ing(i) + a*mg(i);
    g(i+1) = (1/(tau1-tau2)*(tau1*(1-a)-tau2*(1-b)))*ing(i) + b*g(i) + tau1/(tau1-tau2)*(a-b)*mg(i);
    %g(i+1) = (1-b)*mg(i) + b*g(i);
    if(g(i+1)<xx)
        outg(i+1)=1;
    else
        outg(i+1)=-qm*(1-exp(-(exp(g(i+1))-1)/qm));
    end
end

x1=-10:0.001:xx;
x2=xx:0.001:10;
y1=x1*0-1;
y2=qm*(1-exp(-(exp(x2)-1)/qm));
