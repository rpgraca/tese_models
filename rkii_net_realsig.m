clear

sigmoid;
sigmoid_inv;
sigmoid_step_e = Qe(2,1)-Qe(1,1);
sigmoid_min_e = Qe(1,1);
sigmoid_step_i = Qi(2,1)-Qi(1,1);
sigmoid_min_i = Qi(1,1);

w1 = 220;
w2 = 720;
duty = 1/1000;
fsamp = 2000;
delta = duty/fsamp;
tau1 = duty/w1;
tau2 = duty/w2;
a = exp(-delta/tau1);
b = exp(-delta/tau2);
period=0.25*fsamp;
N = 3;

wie = 4e-6;
wei = 4e-6;
wii = 1e-6*(ones(N)-eye(N));
wee = 1e-6*[0 1 0; 1 0 1; 0 1 0];


Ce = 2.1e-12;
Ci = 2.1e-12;
kgm = wie/Ce;
kmg = wei/Ci;
kgg = wii/Ce;
kmm = wee/Ci;

fin = 100;

%in = sin(2*pi*fin*t);
in(1,:) = 0.1 * [ones(1,period) zeros(1,period) ones(1,period) zeros(1,period)];
in(2,:) = 0.1 * [zeros(1,period/2) ones(1,period) zeros(1,period) ones(1,period) zeros(1,period/2)];
in(3,:) = 0.1 * [zeros(1,2*period) ones(1,2*period)];
t = 0:1/fsamp:(length(in))/fsamp;

m(:,1) = zeros(3,1);
mm(:,1) = zeros(3,1);
outm(:,1) = zeros(3,1);
g(:,1) = zeros(3,1);
mg(:,1) = zeros(3,1);
outg(:,1) = zeros(3,1);

for i = 1:length(in)
    for j = 1:N
        inm(j,i) = (in(j,i)+kmg*outg(j,i) + kmm(j,:)*outm(:,i));
        mm(j,i+1) = (1-a)*inm(j,i) + a*mm(j,i);
        m(j,i+1) = (1/(tau1-tau2)*(tau1*(1-a)-tau2*(1-b)))*inm(j,i) + b*m(j,i) + tau1/(tau1-tau2)*(a-b)*mm(j,i);
        %m(i+1) = (1-b)*mm(i) + b*m(i);
        index = round((m(j,i+1)-sigmoid_min_e)/sigmoid_step_e + 1);
        outm(j,i+1)=Qe(index,2);
        ing(j,i) = kgm*outm(j,i) + kgg(j,:)*outg(:,i);
        mg(j,i+1) = (1-a)*ing(j,i) + a*mg(j,i);
        g(j,i+1) = (1/(tau1-tau2)*(tau1*(1-a)-tau2*(1-b)))*ing(j,i) + b*g(j,i) + tau1/(tau1-tau2)*(a-b)*mg(j,i);
        %g(i+1) = (1-b)*mg(i) + b*g(i);
        index = round((g(j,i+1)-sigmoid_min_i)/sigmoid_step_i + 1);
        outg(j,i+1)=Qi(index,2);
    end
end
