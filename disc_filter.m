A = 720;
B = 220;
fsamp = 2000;
duty = 1/1000;
tau1 = duty/A;
tau2 = duty/B;
a = exp(-(duty/fsamp)/tau1);
b = exp(-(duty/fsamp)/tau2);

fin = 60;
t = 0:1/fsamp:6/fin;
in = 0.1 * sin(2*pi*fin*t);

vo(1) = 0;
vm(1) = 0;

for i = 1:length(t)
    vo(i+1) = in(i) * (1/(tau1-tau2)) * (tau1*(1-a)-tau2*(1-b)) + vo(i)*b + vm(i)*tau1/(tau1-tau2)*(a-b);
    vm(i+1) = in(i) * (1-a) + vm(i) * a;
end
